- [x] Latex template.
- [ ] Prelims
- [ ] - Review
- [ ] - Optimal Control
- [ ] - Codes
- [ ] Problem
- [ ] - Research
- [ ] - Simulation
- [ ] Optimal Control Model
- [ ] - Modelling & Coding
- [ ] - Input values and results
- [ ] Conclusion
- [ ] Suggestions
- [ ] Thesis Codes
- 

Feasibility
What we want:
We want a converging sequence, because we don't want an unending loop.

Optimal Control:
We can be sure of a solution with Basic Optimal Control Problems.
We will start with this problem.