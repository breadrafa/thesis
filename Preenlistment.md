IMPORTANT NOTICE for students who Preenlist but eventually decide NOT to push through with their enrollment:

Please be reminded to file for WITHDRAWAL OF ENLISTMENT if:

You have been granted classes during the Preenlistment and/or regular enlistment period, but have decided NOT TO PUSH THROUGH with your enrollment.

To process this online, go to the VIEW MODE of the PREENLISTMENT MODULE of your CRS account and click the button "Withdraw Enlisted Classes".

The Withdrawal of Enlistment CANCELS ALL CLASSES GRANTED during the Preenlistment and/or regular enlistment, and cannot be undone.

This facility will be available only up to January 10 2020. Failure to cancel enlistment by this date means that you intend to pursue your enrollment, which means that:

You are EXPECTED TO PAY THE MATRICULATION FEES covering all these classes, failing which an ACCOUNTABILITY will be incurred. A student with an accountability will be tagged as INELIGIBLE TO ENROLL in the next term; and

You WILL BE GIVEN GRADES FOR ALL THE CLASSES, whether or not these have been paid for.

Also, please be reminded that failure to withdraw enlistment in classes prevents other students from enrolling in those classes. It is for this reason that you are made accountable for your enlistment in these classes.

Furthermore, please file for LEAVE OF ABSENCE (LOA) or RESIDENCE if you are not going to enroll in specific classes during any particular term. The period of residence may also be used to complete an “INC”, remove a grade of “4.00” or comply with any requirement which requires a mark. (e.g. Thesis requirement)



Thank you.