# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 12:08:19 2017

@author: perea

Chapter 4 code1.m counterpart

TODO: module to make it a reusable function

"""
import numpy as np
import matplotlib.pyplot as pl

def code1(A, C, x0, M1, M2):
    """
    Input Conditions:
        A, B, C are constants. x(0) = x0
        B = $`-0.5\frac{ \partial^2 H }{ \partial u} > 0`$ : (i.e. maximizes)
            concavity of Hamiltonian.
    """
    test = -1
    
    delta = 0.001
    
    N = 1000
    t = np.linspace(0,1,N+1)
    h = 1/N
    h2, h6 = h/2, h/6
    
    u, x, lamda = np.zeros((3,N+1))
    x[0] = x0

    while(test < 0):
        oldu, oldx, oldl = u, x, lamda
        
        # solve for x
        for i in range(0,N):
            # i \in \[ 0, N-1 \]
            ua, ub, xa = u[i], u[i+1], x[i]
            um = 0.5 * (ua + ub)
            k1 = -0.5 * xa**2 + C * ua
            k2 = -0.5 * (xa + h2*k1)**2 + C * um
            k3 = -0.5 * (xa + h2*k2)**2 + C * um
            k4 = -0.5 * (xa + h*k3)**2 + C * ub
            x[i+1] = xa + h6*(k1 + 2*k2 + 2*k3 + k4)
        
        # solve for lambda
        for i in range(1,N+1):
            j = -i # j \in reversed(\[-N,...,-1\])
            xb, xa, lb = x[j], x[j-1], lamda[j]
            xm = 0.5 * (xa + xb)
            k1 = -A + lb * xb
            k2 = -A + (lb - h2*k1)*xm
            k3 = -A + (lb - h2*k2)*xm
            k4 = -A + (lb - h*k3)*xa
            lamda[j-1] = lb - h6*(k1 + 2*k2 + 2*k3 + k4)
           
        u1 = np.minimum(M2, np.maximum(M1,C*lamda/2))
        u = 0.5 * (u1 + oldu)
        
        temp1 = delta*sum(abs(u)) - sum(abs(oldu-u))
        temp2 = delta*sum(abs(x)) - sum(abs(oldx-x))
        temp3 = delta*sum(abs(x)) - sum(abs(oldl-lamda))

        test = min(temp1, temp2, temp3)
    
    return np.array([t, x, lamda, u])

t, x, l, u = code1(1,4,1,-1,2)
pl.plot(t,x, 'b-', t,l, 'r-', t, u, 'g-')