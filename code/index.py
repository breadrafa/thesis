# -*- coding: utf-8 -*-
"""
Created on Sat Sep  2 04:44:47 2017
4th order Runge Kutta
@author: perea
resource: 
    ----------------------------------------------------------------------------------------------------------------------------------------------
    src code: 
    http://mtweb.cs.ucl.ac.uk/mus/lib/python2.7/numpy-1.7.0b2-py2.7-linux-x86_64.egg/numpy/polynomial/polynomial.py
    
    doc: %SCIPYDOCS% = "https://docs.scipy.org/doc/numpy-1.10.0/reference/generated"
        [1] polyval3d 
            %SCIPYDOCS% + "/numpy.polynomial.polynomial.polyval3d.html"    
        [2] polygrid3d
            %SCIPYDOCS% + "/numpy.polynomial.polynomial.polygrid3d.html"
"""
import numpy as np
import matplotlib.pyplot as plt

npx = np.polynomial.polynomial

def get_rk4(fxu, x0,  u, t, N = 1000, forward = True):
    """
    input:
        x0 = Mn1(R) vector
        fx = x' with basis np.1 x 
    """
    u = np.array(u)         # defaults to zeros, i.e., no action taken.
    x = np.zeros(N+1)       # init
    x[0] = x0               # given
    m = 1 if forward else -1
    my_range = range(0,N) if forward else range(1,N+1)
    t = np.linspace(t[0], t[-1], 2*N+1)
    # half = 0/5*h            # half step
    for i in my_range:
        j = m*i
        um = 0.5*(u[j]+u[j+m])                  # midpt between 2 u pts
        ta,tm,tb = t[2*j], t[2*j+m], t[2*j]     # replaces t, t+h/2, t+h

        k1 = npx.polyval3d(x[j],         u[j],    ta,   fxu)
        k2 = npx.polyval3d(x[j]+0.5*m*k1,  um,    tm,   fxu)
        k3 = npx.polyval3d(x[j]+0.5*m*k2,  um,    tm,   fxu)
        k4 = npx.polyval3d(x[j]+m*k3,    u[j+m],  tb,   fxu)

        xdelta = (k1 + 2*k2 + 2*k3 + k4)/(6*N)  # note h = 1/N 
        x[j+m] = x[j] + m*xdelta
    
    return x

def code1(A,B,C,x0, t0 = 0, t1 = 1, N = 300):
    if B > 0:
        test = -1
        delta = 0.001
        t = np.linspace(t0, t1, N+1)
        u, x, l = np.zeros((3,N+1))
        ctr = 1
        while(test < 0):
            oldx, oldu, oldl = x, u, l
            # x' = -0.5 x^2 + Cu
            x = get_rk4([[0,0,-1/2], [C,0,0]], x0, u, t, N)
            # l' = -A + l * x
            l = get_rk4([[-A, 0],[0,1]], 0, x, t, N, forward = False)
            u1 = C*l / (2*B)
            u = 0.5*(u1 + oldu)

            temp1 = delta*sum(abs(u)) - sum(abs(oldu-u))
            temp2 = delta*sum(abs(x)) - sum(abs(oldx-x))
            temp3 = delta*sum(abs(l)) - sum(abs(oldl-l))

            test = min(temp1, min(temp2, temp3))
            ctr = ctr + 1
        y = np.array([t, x, l, u])
        return y
    else:
        return "Try again with a positive B"
# Chapter 5
t, x, l, u = code1(1,1,4,1)
print(t,x,u,l)
# plt.plot(t, x, "gs", t, l, "y^", t, u, "b-")
t, x, l, u = code1(1,2,4,1)
# plt.plot(t, x, "gs", t, l, "y^", t, u, "b-")
t, x, l, u = code1(2,4,4,1)
plt.plot(t, x, "gs")

## Exercise 5.1

def exam1p1(x0=1, t0 = 0, t1 = 1, N = 300):
    test = -1
    delta = 0.001
    t = np.linspace(t0, t1, N+1)
    u, x, l = np.zeros((3,N+1))
    ctr = 1
    while(test < 0):
        oldx, oldu, oldl = x, u, l
        # x' = x + u
        x = get_rk4([[0,1], [1,0]], x0, u, t, N)  
        # l' = -l
        l = get_rk4([[0,-1],[0,0]], 0, x, t, N, forward = False)
        u1 = -0.5*l
        u = 0.5*(u1 + oldu)

        temp1 = delta*sum(abs(u)) - sum(abs(oldu-u))
        temp2 = delta*sum(abs(x)) - sum(abs(oldx-x))
        temp3 = delta*sum(abs(l)) - sum(abs(oldl-l))

        test = min(temp1, min(temp2, temp3))
        ctr = ctr + 1
    y = np.array([t, x, l, u])
    return y

t, x, l, u = exam1p1(N=10000)
print(t,x,u,l)
plt.plot(t, x, "gs")
