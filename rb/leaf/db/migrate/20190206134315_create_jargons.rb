class CreateJargons < ActiveRecord::Migration[5.2]
  def change
    create_table :jargons do |t|
      t.string :name
      t.string :description
      t.text :definition

      t.timestamps
    end
  end
end
