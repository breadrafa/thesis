# Basic Optimal Control Problem
Below is a basic example of optimal control problem:

```math
\begin{array}{rl}
& \max_u \int_{t_0}^{t_1} f\left(t,x(t),u(t)\right)dt + \phi\left(x(t_1)\right) \\
\text{subject to: }
& x^\prime(t) = g\left(t,x(u),u(t)\right) \\
& x(t_0) = x_0 
\end{array}

```

We let $`J(u) = \int_{t_0}^{t_1} f\left(t,x(t),u(t)\right)dt + \phi\left(x(t_1)\right)`$, which is the value of the objective functional
to the control $`u`$, a piecewise continuous function, and where
x(t) is a piecewise differentiable function, the associated state of the control $`u`$.

It can be seen that given a control $`u(t)`$, the constraints (or the statements next to "subject to") is an initial value problem (IVP), where the state $`x(t)`$ is the solution. Note that $`f\left(t,x(t),u(t)\right)`$ and $`g\left(t,x(t),u(t)\right)`$, are piecewise continuous functions.

We may think of $`g`$ as the historical dependency of the change of state, that is, it depends on the current state $`x`$ and control $`u`$ at every particular time $`t \in (t_0, t_1)`$. The function $`f`$ depends similarly, and the objective function J(u) is the summation or integration of the function $`f`$ all through out the time interval added to the piecewise differentiable function $`\phi`$ of the final state $`x(t_1)`$.

In summary, an optimal control problem, is an optimization problem (in this case maximization) in which the current state(s) affect/s the rate of change of the same state as well as the objective function, which we manipulate through some control(s).

We will observe necessary conditions of an optimal control $`u^*`$, and the optimal state $`x^*`$ associated with it.

Let $`u^\epsilon(t) = u^*(t) + \epsilon h(t)`$, for some piecewise continuous variational function h(t). Our choice of h(t) makes $`u^\epsilon`$ piecewise continuous, a valid control which gives a different IVP, and thus gives a state $`x^\epsilon`$ associated to it.

```math
\begin{array}{l}
\frac{d}{dt}x^\epsilon(t) = g\left(t,x^\epsilon(u),u^\epsilon(t)\right) \\
x^\epsilon(t_0) = x_0
\end{array}
```

Note that

```math
\frac{du^\epsilon}{d\epsilon} = \lim_{\epsilon\to 0} u^\epsilon = \lim_{\epsilon\to 0} \frac{u^\epsilon(t) - u^*(t)}{\epsilon} = \lim_{\epsilon\to 0} \frac{\epsilon h(t)}{\epsilon} = h(t)
```

Thus, as $`\epsilon \to 0`$, $`u^\epsilon \to u^*`$, and so does the IVPs converge and $`x^\epsilon \to x^*`$. Thus, $`\left.\frac{dx^\epsilon}{d\epsilon}\right|_{\epsilon=0}`$ exists.

Now, we recall the second form of the Fundamental Theory of Algebra (FTA), for some piecewise differentiable function $`\lambda(t)`$,

```math
\begin{array}{rl}
& \int_{t_0}^{t_1} \frac{d}{dt}\left[\lambda(t)x^\epsilon(t)\right] dt = \lambda(t_1)x^\epsilon(t_1) - \lambda(t_0)x^\epsilon(t_0) \\
\Leftarrow & \int_{t_0}^{t_1} \frac{d}{dt}\left[\lambda(t)x^\epsilon(t)\right] dt - \lambda(t_1)x^\epsilon(t_1) + \lambda(t_0)x^\epsilon(t_0)=0 \\
\end{array}
```
From the second IVP with solution $`x^\epsilon(t)`$, we have the zero expression:

```math
\int_{t_0}^{t_1} \lambda(t)g\left(t, x^\epsilon(t), u^\epsilon(t)\right) + \lambda^\prime(t) x(t) dt - \lambda(t_1)x^\epsilon(t_1) + \lambda(t_0)x_0=0
```

We will add above to the value of the objective function,

```math
\begin{array}{rl}
J\left(u^\epsilon\right) & = \int_{t_0}^{t_1} f\left(t,x^\epsilon(t),u^\epsilon(t)\right)dt + \phi\left(x^\epsilon(t_1)\right) + \int_{t_0}^{t_1} \lambda(t)g\left(t, x^\epsilon(t), u^\epsilon(t)\right) + \lambda^\prime(t) x(t) dt - \lambda(t_1)x^\epsilon(t_1) + \lambda(t_0)x_0 \\
& = \int_{t_0}^{t_1} f\left(t,x^\epsilon(t),u^\epsilon(t)\right) + \lambda(t)g\left(t, x^\epsilon(t), u^\epsilon(t)\right) + \lambda^\prime(t) x(t) dt  + \phi\left(x^\epsilon(t_1)\right) - \lambda(t_1)x^\epsilon(t_1) + \lambda(t_0)x_0
\end{array}
```
Since $`\lambda`$, $`\lambda^\prime`$, $`x`$, $`f`$, and $`g`$ are piecewise continuous functions, thus, we can 
For any control $`u^\epsilon`$, objective functional value $`J(u^\epsilon) \leq J(u^*)`$, by optimality of control $`u^*`$.
Thus, the total differential of $`J`$ is zero.

```math
\begin{array}{rcl}
0 & = & \left.\frac{dJ(u^\epsilon)}{d\epsilon}\right|_{\epsilon = 0}\\
& = & \left. \frac{\partial}{\partial\epsilon} 
    \left[ \int_{t_0}^{t_1} f \left(t,x^\epsilon(t),u^\epsilon(t)\right) 
        + \lambda(t)g\left(t, x^\epsilon(t), u^\epsilon(t)\right) 
        + \lambda^\prime(t) x(t) dt 
        \right] \right|_{\epsilon=0}
+ \left. \frac{\partial}{\partial\epsilon}
    \left[\phi\left( x^\epsilon(t_1)\right)\right]
    \right|_{\epsilon=0} 
- \left. \frac{\partial}{\partial\epsilon}
    \left[\lambda(t_1)x^\epsilon(t_1)\right]
    \right|_{\epsilon=0} + 0 \\
& = & \int_{t_0}^{t_1} \left[
    \left. \frac{\partial}{\partial\epsilon} f\left(t,x^\epsilon(t),u^\epsilon(t)\right)
        \right|_{\epsilon=0}
    + \left. \frac{\partial}{\partial\epsilon} \left[\lambda(t)g\left(t, x^\epsilon(t), u^\epsilon(t)\right)\right]
        \right|_{\epsilon=0} 
    + \left. \frac{\partial}{\partial\epsilon}\lambda^\prime(t) x(t)
        \right|_{\epsilon=0}
    \right] dt 
+ \left. \frac{\partial}{\partial\epsilon} \left[\phi\left( x^\epsilon(t_1)\right)\right]
    \right|_{\epsilon=0} 
- \left. \frac{\partial}{\partial\epsilon} \left[\lambda(t_1)x^\epsilon(t_1)\right]
    \right|_{\epsilon=0} \\
& = & \int_{t_0}^{t_1} \left[
f_x \left.\frac{\partial x^\epsilon(t)}{\partial\epsilon} \right|_{\epsilon=0} + 
f_u \left.\frac{\partial u^\epsilon(t)}{\partial\epsilon} \right|_{\epsilon=0} + 
\lambda(t)\left(
g_x \left.\frac{\partial x^\epsilon}{\partial\epsilon} \right|_{\epsilon=0} + 
g_u \left.\frac{\partial u^\epsilon}{\partial\epsilon} \right|_{\epsilon=0}
\right) + 
\lambda^\prime(t) \left.\frac{\partial x^\epsilon(t)}{\partial\epsilon} \right|_{\epsilon=0} 
\right] dt \\
& & + \phi^\prime(x^*(t_1))\left.\frac{\partial x^\epsilon(t_1)}{\partial\epsilon} \right|_{\epsilon=0} - \lambda(t_1)\left.\frac{\partial x^\epsilon(t_1)}{\partial\epsilon} \right|_{\epsilon=0} \\
& = & \int_{t_0}^{t_1} \left(
f_x + \lambda g_x + \lambda^\prime
\right)\left.\frac{\partial x^\epsilon}{\partial\epsilon} \right|_{\epsilon=0} +
\left(
f_u + \lambda g_u
\right)\left.\frac{\partial u^\epsilon}{\partial\epsilon} \right|_{\epsilon=0}
+ \left(\phi^\prime(x^*(t_1))
- \lambda(t_1)             \right)\left.\frac{\partial x^\epsilon(t_1)}{\partial\epsilon} \right|_{\epsilon=0} \\
\end{array}
```

by defining 

```math
\lambda(t_1) = \phi^\prime(x^*(t_1)),
```
we make the coordinate of $`\left.\frac{\partial x^\epsilon(t_1)}{\partial\epsilon} \right|_{\epsilon=0}`$ zero (i.e. the transversality condition).
Similarly, we define 
```math
lambda^prime(t) = -f_x - \lambda g_x,
```
(i.e. the adjoint equation).

And from our note earlier,

```math
\begin{array}{rl}
0 & = \left.\frac{dJ(u^\epsilon)}{d\epsilon}\right|_{\epsilon = 0}\\
& = \int_{t_0}^{t_1} \left(f_u + \lambda g_u
\right)h
\end{array}
```

Now by optimality of $`J(u^\epsilon)`$ at $`\epsilon = 0`$. is true for all variational functions $`h`$. In particular, if $`h(t) = f_u + \lambda g_u`$ then,
```math
f_u + \lambda g_u \equiv 0,
```
(i.e. the optimality condition).

# Bounded optimal control
Earlier, we established the necessary conditions whenever an optimal control $`u^*`$ and the associated state $`x^*`$ exist,
<ul>
<li>Transversality condition so that the coefficient of $`\left.\frac{\partial x^\epsilon(t_1)}{\partial\epsilon}\right|_{\epsilon=0}`$ is zero:

```math
\lambda(t_1) = \phi^\prime(x^*(t_1));
```
</li> 
<li>Adjoint equation so that the coefficient of $`\left.\frac{\partial x^\epsilon(t)}{\partial\epsilon}\right|_{\epsilon=0}`$ is zero:

```math
\lambda(t_1) = \phi^\prime(x^*(t))
```
</li>
<li>Optimality condition so that $`\left.\frac{dJ(u^\epsilon(t))}{d\epsilon}}\right|_{\epsilon=0}`$ regardless of choice of $`h(t)`$:

```math
f_u(t, x^*(t),u^*(t)) + \lambda g_u(t, x^*(t),u^*(t)) = 0
```
</li>
</ul>

However, in real life, there are limitations to what we can control, like limitation on resources and physical limitations.

Now, we will consider a bounded optimal control problem

```math
\max_u J(u) = \max_u \int_{t_0}^{t_1} f(t, x(t), u(t)) dt + \phi(x(t_1))
```
subject to:

```math
\begin{array}{l}
x^\prime(t) = g(t, x(t), u(t)) \\
x(t_0) = x_0 \\
a \leq u(t) \leq b.
\end{array}
```

Let $`\epsilon > 0`$.
Define

```math
u^\epsilon(t) = u^*(t) + \epsilon h(t),
```
where $`u^*(t)`$ is the bounded optimal control and 
Note that with our choice of $`\epsilon`$ it follows that if control $`u^\epsilon(t) > u^*(t)`$ at some $`t`$ if and only if $`h(t)`$ is positive.

We will proceed similarly, but, instead of getting the two sided limits, we'll get pointwise derivatives in $`t \in (t_0,t_1)`$ from the right of zero.

```math
0 \geq \frac{dJ(u^\epsilon)}{d\epsilon}  := \lim_{\epsilon\to0^+} \frac{J(u^\epsilon) - J(u^*)}{\epsilon}
```

The inequality means we are allowing minimal deviation from the ideal optimal control $`u^k(t)`$, with associated optimal state $`x^k(t)`$. This is the case where the ideal is beyond the lower bound $`a`$ or upper bound $`b`$, the superscript $`(0)`$ refers to the optimal control and state pair, wherein we ignore the bounds of control $`u(t)`$. Without loss of generality if we have $`t_0 < t_i < t_j < t_1`$ and $`u^k(t_i) < a`$ and  $`u^k(t_j) > b`$. Consider the case wherein $`u(t)`$ is the amount, to be injected, of a cure to a bacterial disease in milligrams and $`x(t)`$ the size of the colony of bacteria. In this case we let $`a = 0`$ since we can't suck the cure out of the patient. Thus, we choose to do nothing $`u^*(t_i) = a = 0`$ and wait. By the historical dependency of the state given by the constraints, 

```math
\frac{dx^*(t_i)}{dt} = g(t, x^*(t_i), u^*(t_i)) \neq g(t, x^k(t_i), u^k(t_i)) = \frac{dx^k(t_i)}{dt}.
```
Therefore, $`x^*(s) \neq x^k(s)`$ for some point of continuity in $`s \in (t_i, t_i+\delta)`$, it can be shown similarly that the $`u^*(t)`$ will not be as simple as a restriction of the solution from the previous section. Think of it as in order to accommodate for the previous deviation due to the bound, adjustments shall be made so that even when $`a < u^k(t) < b`$, $`u^*(t)`$ is not necessarily equal to $`u^k(t)`$
 
Now we will proceed similar to the previous section. Add the same zero expression given by the Fundamental Theorem of Algebra to $`J(u^\epsilon)`$ and differentiate:

```math
\begin{array}{rl}
0 & \geq \int_{t_0}^{t_1} 
\left(\lambda^\prime + f_x + \lambda g_x \right)
\left.\frac{\partial x^\epsilon(t)}{\partial\epsilon}\right|_{\epsilon = 0^+}
+
\left(f_u + \lambda g_u \right)
\left.\frac{\partial u^\epsilon(t)}{\partial\epsilon}\right|_{\epsilon = 0^+} dt
+
\left(\phi^\prime(x(t_1)) - \lambda(t_1)\right)
\left.\frac{\partial x^\epsilon(t_1)}{\partial\epsilon}\right|_{\epsilon = 0^+}

\end{array}

```

Since $`\left.\frac{\partial u^\epsilon(t)}{\partial\epsilon}\right|_{\epsilon=0^+} = h(t)`$. We can simplify above by choosing the same (adjoint equation) $`\lambda`$ so that the coefficient of $`\left.\frac{\partial x^\epsilon(t)}{\partial\epsilon}\right|_{\epsilon = 0^+} `$ to be

```math
\lambda^\prime(t) = -f_x(t, x^*(t),u^*(t)) - \lambda(t)g_x(t, x^*(t),u^*(t))
```
with the same endpoint value (or transversality condition) of 
```math
\lambda(t_1) = \phi^\prime(x(t_1)).
```

Unlike the unbounded case, $`\frac{dJ(u^\epsilon)}{d\epsilon}`$ may be negative here,

```math
0 \geq \int_{t_0}^{t_1} \left(f_u(t, x^*(t), u^*(t)) + \lambda(t) g_u(t, x^*(t), u^*(t)) h(t) \right)dt.
```

First we consider controls equal or near the lower bound.
Let $`s`$ be a point of continuity of $`u^*`$, such that $`a \leq u^*(t) < b`$. Hence, there is an open interval $`I \subseteq (t_0, t_1)`$, such that $`s \in I`$. Suppose $`f_u + \lambda g_u > 0`$.
Restrict $`\epsilon \in (0, 1)`$ and define 

```math
h(t) = \left\{
\begin{array}{ll}
b-M & if\;t \in I\\
0 & if\;t\not\in I
\end{array}\right.
```
where $`M = \max \left\{u^*(t)| t \in I \right\}`$
So that we have $`a \leq u^*(t) + \epsilon h(t) \epsilon \leq b`$ a bounded control $`u^\epsilon(t)`$ which is not optimal.

Clearly, $`h(t) > 0`$, then,
\int_{t_0}^{t_1} \left( f_u + \lambda g_u \right) h = \int_I \left( f_u + \lambda g_u \right) h > 0.
which is a contradiction to our assumption that $`f_u + \lambda g_u > 0`$.

Therefore, $`f_u + \lambda g_u \leq 0`$ when $`u^*(t) \neq b`$

Next is when $`u^*(t) \neq 0`$,
Let $`s`$ be a point of continuity of $`u^*`$, such that $`a < u^*(t) \leq b`$. Hence, there is an open interval $`I \subseteq (t_0, t_1)`$, such that $`s \in I`$. Suppose $`f_u + \lambda g_u < 0`$.
Restrict $`\epsilon \in (0, 1)`$ and define 

```math
h(t) = \left\{\begin{array}{ll}
a-m & if\;t \in I\\
0 & if\;t\not\in I
\end{array}\right.
```
where $`m = \min \left\{u^*(t)| t \in I \right\}`$
So that we have $`a \leq u^*(t) + \epsilon h(t) \epsilon \leq b`$ a bounded control $`u^\epsilon(t)`$ which is not optimal.

Clearly, $`h(t) < 0`$, then,
\int_{t_0}^{t_1} \left( f_u + \lambda g_u \right) h = \int_I \left( f_u + \lambda g_u \right) h > 0.
which is a contradiction to our assumption that $`f_u + \lambda g_u < 0`$.

Therefore, $`f_u + \lambda g_u \geq 0`$ whenever $`u^*(t) \neq a`$

Equivalently, by conjunction and contrapositive.
if $` f_u + \lambda g_u < 0`$ at $`t`$, then $` u^*(t) = a`$;
if $` f_u + \lambda g_u = 0`$ at $`t`$, then $` a \leq u^*(t) \leq b`$, and;
if $` f_u + \lambda g_u > 0`$ at $`t`$, then $` u^*(t) = b`$,
for all points of continuity $`t`$ of $`u^*(t)`$. (optimality condition).

Now these necessary conditions for $`x^*`$ and $`\lambda`$ of an optimal control $`u^*`$ can be solved by forming the Hamiltonian,

```math
H(t, x, u, \lambda) = f(t,x,u) + \lambda g(t,x,u) 
```
So that,

```math
\begin{array}{rllll}
x^\prime(t) &= g(t,x,u) &= \frac{\partial H}{\partial \lambda} &,&x(t_0) = x_0 \\ 
\lambda(t) &= -(f_x+\lambda g_x)&= -\frac{\partial H}{\partial x} &,&\lambda(t_1) = \phi^\prime(x(t_1))
\end{array}
```
and
```math
\left\{\begin{array}{ll}
u^* = a & \text{if }\frac{\partial H}{\partial u} < 0\\
a \leq u^* \leq b & \text{if } \frac{\partial H}{\partial u} = 0\\
u^* = b & \text{if } \frac{\partial H}{\partial u} > 0.
\end{array}
```
