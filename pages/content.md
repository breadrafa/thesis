# Review

## First Fundamental Theorem of Calculus
If $`f`$ is continuous on a closed interval [a,b] and $`F`$ is the indefinite integral of $`f`$ on $`[a,b]`$, then

```math
\int_a^b f(x)dx = F(b) - F(a).
```

## Corollary
If $`f`$ is piecewise differentiable on a closed interval [a,b] and $`F`$ is the indefinite integral of $`f`$ on $`[a,b]`$, then

```math
\int_a^b f(x)dx = F(b) - F(a).
```

### Proof
Let $`{a,x_1, \ldots, x_{n-1}, b}`$ be a partition of $`[a,b]`$ with $`x_0 = a`$ and $`x_n = b`$ $`\newline`$
Define $`f_i = f|_{[x_{i-1},x_i]}`$, $`F(x) = \int_a^x f(t) dt`$ 
and $`F_i(x) = \int_a^{x_i} f(t)dt`$ for all $`i=1,2,\ldots,n`$.

Note that $`f_i`$ is continuous on closed interval $`[x_{i-1}, x_i]`$ thus,

```math
\int_{x_{i-1}}^{x_i} f_i(x)dx = \int_{x_{i-1}}^{x_i} f(x)dx = F_i(x_i) - F_i(x_{i-1})
```
for all $`i=1,2,\ldots,n`$. Since F is well defined, $`F(x_i) = F_i(x_i) = F_{i+1}(x_i)`$ for all $`i = 1,\ldots, n-1`$. Thus we have

```math

\begin{aligned}
\int_a^b f(x)dx &= \sum_{i=1}^n \int_{x_{i-1}}^{x_i} f_i(x)dx \\
&= \sum_{i=1}^n F(x_i) - F(x_{i-1}) \\
&= F(b) - F(a).
\end{aligned}

```

# Optimal Control Problem

## Basic Optimal Control Problem
A basic optimal control problem is a system of the following
```math
\max_u J(u) = \int_{t_1}^{t^2} f(t,x(t),u(t)) dt
```
```math
\begin{aligned}
\text{subject to: } &
  x^\prime(t) = g(t, x(t), u(t)) \\
& x(t_0) = x_0 \text{ and } x(t_1) \text{ free.}
\end{aligned}
```
where $`u: I\mapsto \mathbb{R}`$ is a piecewise continuous function over time with $`I = [t_1,t_2]`$, <br>
and $`x: I\mapsto \mathbb{R}`$, $`f`$ and $`g`$ are continuous differentiable functions. <br>
We will call $`J`$ the objective function, $`u`$, the control, and $`x`$ the state. 

The goal is to either maximize or minimize the objective function $`J`$ by solving the control $`u`$. <br>
The state $`x`$ will depend on our control function (rule) $`u`$.
The portion next to "subject to" is called collectively as constraints. $`x(t_1)`$ free means there is no constraints. Omitting this portion will not affect the problem.

## Necessary Conditions of an Optimal Control

~~We can think of $`u`$ as our interaction with the problem over time. A simple example is pushing a cart forward when in line to pay. Control $`u`$ can be the pushing action. Piecewise, because we may choose not to push continually. The state $`x`$ may represent the position of the cart. No matter how we push, we cannot make the cart teleport, thus continuous. We can only hope to affect a state's acceleration, not move it by sheer will. The solution in this example is almost a given, don't leave the line, but what if you forgot to pick an item in your list.~~

This study is an example of a proof by construction. We will limit our scope such that a feasible unique solution exists. 

We will start by finding the properties or necessary conditions of an optimal control system with a unique optimal control solution.

Let $`u^*`$ be the optimal control, and $`x^*`$ be the corresponding optimal state. This means that $`J(u^*) \geq J(u)`$, for all control $`u`$.
Let $`h(t)`$ is a piecewise continuous function, we will later call the variational function, and $`\epsilon`$ a constant.<br>
We can define a $`u^\epsilon(t) = u^* + \epsilon h(t)`$ an epsilon ($`\epsilon`$) variation of our optimal control $`u^*`$. 
Note that $`u^\epsilon`$ is also a valid control, since it's a piecewise continuous function.
We will denote the corresponding state $`x`$ as $`x^\epsilon`$, and the corresponding $`x^\prime`$ as $`x^\epsilon`$ satisfying
```math
\frac{dx^\epsilon}{dt} = g(t, x^\epsilon(t), u^\epsilon(t).
```
Note that $`x^\epsilon`$ depends on $`u^\epsilon`$, and thus, as $`\epsilon`$ diminishes, we have $`u^\epsilon \to u^*`$ and $`x^\epsilon \to x^*`$.
It follows that we can get the total differential of $`g`$
```math
\frac{dg}{d\epsilon} =\frac{\partial x^\epsilon}{\partial\epsilon} + \frac{\partial u^\epsilon}{\partial\epsilon}
```
Above establishes the existence of $`\frac{\partial x^\epsilon}{d\epsilon} \in \mathbb{R}`$ since
```math
\frac{\partial u^\epsilon}{\partial\epsilon} = \lim_{\epsilon\to0} \frac{u^\epsilon -u^*}{\epsilon} = \lim_{\epsilon\to0}\frac{\epsilon h(t)}{\epsilon} = h(t)
```

#### Adjoint
We will introduce an adjoint $`\lambda(t)`$, piecewise differentiable to establish later the dependence of the objective function to the constraints.
```math
\int_{t_0}^{t_1}D^\prime\left[\lambda(t)x^\epsilon(t)\right]dt = \lambda(t_1)x^\epsilon(t_1) - \lambda(t_0)x^\epsilon(t_0)
```
by the Fundamental Theorem of Algebra.
```math
\begin{array}{rcl}
0 & = & \int_{t_0}^{t_1}D^\prime\left[\lambda(t)x^\epsilon(t)\right]dt - \lambda(t_1)x^\epsilon(t_1) + \lambda(t_0)x^\epsilon(t_0)
  & = & \int_{t_0}^{t_1}\lambda(t)g(t,x^\epsilon(t),u^\epsilon(t)) + \lambda^\prime(t)x^\epsilon(t) dt - \lambda(t_1)x^\epsilon(t_1) + \lambda(t_0)x_0 
\end{array}
```
the first equality is true by addition property of equality, then we apply the derivative substituting the constraints at $`u^\epsilon`$, i.e. all states starts at $`x_0`$ and the derivative of $`x^\epsilon`$ we derived above<br>
Then, we add this zero-valued expression to the objective function at $`u^\epsilon`$
```math
J(u^\epsilon) = \int_{t_0}^{t_1} 
  f(t,x^\epsilon(t), u^\epsilon(t)) + \lambda(t)g(t,x^\epsilon(t),u^\epsilon(t)) + \lambda^\prime(t)x^\epsilon(t) dt
- \lambda(t_1)x^\epsilon(t_1)
+ \lambda(t_0)x_0
```
#### As $`\epsilon`$ approaches zero
Now, since $`J`$ depends on our choice of $`u`$ and we defined control $`u^\epsilon`$ so that it is optimal at $`\epsilon = 0`$, it follows that
```math
\left.\frac{d}{d\epsilon} J(u^\epsilon)\right|_{\epsilon=0} = \lim_{\epsilon\to0} \frac{J(u^\epsilon)- J(u^*)}{\epsilon} = 0,
```
that is, the optimality condition.
```math
\begin{aligned}
0 & = \left.\frac{d}{d\epsilon} J(u^\epsilon)\right|_{\epsilon=0} \\
  & = \left.\frac{d}{d\epsilon}\left[\int_{t_0}^{t_1} 
    f(t,x^\epsilon(t), u^\epsilon(t)) + \lambda(t)g(t,x^\epsilon(t),u^\epsilon(t)) + \lambda^\prime(t)x^\epsilon(t) dt
- \lambda(t_1)x^\epsilon(t_1)
+ \lambda(t_0)x_0 \right]\right|_{\epsilon=0} \\
  & = \int_{t_0}^{t_1} 
  \left.f_x\frac{\partial x^\epsilon}{\partial\epsilon}\right|_{\epsilon=0} + \left.f_u\frac{\partial u^\epsilon}{\partial\epsilon}\right|_{\epsilon=0}
  + \lambda(t)\left(\left.g_x\frac{\partial x^\epsilon}{\partial\epsilon}\right|_{\epsilon=0} + \left.g_u\frac{\partial u^\epsilon}{\partial\epsilon}\right|_{\epsilon=0}\right)
  + \left.\lambda^\prime(t)\frac{\partial x^\epsilon}{\partial\epsilon} \right|_{\epsilon=0} dt
- \lambda(t_1)
  \left[ \frac{\partial x^\epsilon}{\partial\epsilon} \right|_{\epsilon = 0}
\end{aligned}
```
wherein the parameter of each of $`f_u`$, $`g_u`$, $`f_x`$, and $`g_x`$ is $`(t,x^*(t),u^*(t))`$<br>
Lebesgue Dominated Convergence Theorem allows us to move the limit into the integral.
Reassociating the addends
```math
0 = \int_{t_0}^{t_1}
  \left(f_u + \lambda(t)g_u\right)\left.\frac{\partial u^\epsilon}{\partial\epsilon}\right|_{\epsilon=0} dt
  + \int_{t_0}^{t_1} \left(f_x + \lambda(t)g_x + \lambda^\prime(t)\right)\left.\frac{\partial x^\epsilon}{\partial\epsilon}\right|_{\epsilon=0} dt
- \lambda(t_1)\left.
  \frac{\partial x^\epsilon}{\partial\epsilon}
\right|_{\epsilon = 0}
```
We will consider making the coordinates of 
```math
\left.\frac{\partial x^\epsilon}{\partial\epsilon}\right|_{\epsilon = 0}
``` 
vanish in choosing the adjoint equation to simplify the equation.

### Adjoint Equation
Where $`\lambda(t)`$ satisfies
```math
\lambda^\prime(t) = -(f_x(t,x^*(t), u^*(t)) + \lambda(t)g_x(t,x^*(t), u^*(t)))
```
### Transversality Condition
and 
```math
\lambda(t_1) = 0
```

These conditions reduces the equation to
```math
\begin{aligned}
0 &= \int_{t_0}^{t_1}
  \left(f_u + \lambda(t)g_u\right)\left.\frac{\partial u^\epsilon}{\partial\epsilon}\right|_{\epsilon=0} dt. \\
  &= \int_{t_0}^{t_1}
  \left(f_u + \lambda(t)g_u\right)h(t) dt. \\
  &= \int_{t_0}^{t_1}
  \left(f_u + \lambda(t)g_u\right)^2 dt.
\end{aligned}
```
The second equation is the fact we derived earlier. The last is by letting
```math
h(t) = f_u(t,x^*(t), u^*(t)) + \lambda(t)g_u(t,x^*(t), u^*(t)),
```
since it applies to all piecewise differentible variation function $`h(t)`$.<br>
### Optimality Condition
This means that the optimality condition is equivalent to
```math
0 = f_u(t,x^*(t), u^*(t)) + \lambda(t)g_u(t,x^*(t), u^*(t))
```
for all $`t_0 \leq t \leq t_1`$.

### Hamiltonian H
These equations necessary for optimal control problem to have a solution. We can derive this equations from the Hamiltonian $`H`$, which is defined as follows,
```math
\begin{array}{rcl}
H(t, x, u, \lambda) &=& f(t,x,u) + \lambda g(t,x,u)
&=& \text{ integrand }+\text{ adjoint }*\text{ RHS of DE},
\end{array}
```
that is, the sum of the integrand of the objective function and the product of the adjoint and the right hand side of the differential of x. With this definition, to solve the optimal control $`u^*`$ we only need to solve the following:

### Algorithm
```math
\begin{aligned}
\frac{\partial H}{\partial u} = 0 \text{ at } u^* \implies f_u + \lambda g_u = 0 & \text{ (optimality condition),} \\
\lambda^\prime = -\frac{\partial H}{\partial x} \implies \lambda^\prime = f_x + \lambda g_x & \text{ (adjoint equation), and} \\
\lambda(t_1) = 0 &\text{ (transversality condition)}.
\end{aligned}
```
Combining the given optimal control problem, there is a similarity between $`x`$ and $`\lambda`$. We know their starting and ending points, respectively, and their differential equations. In solving $`x`$ the unknown is $`u`$ while in $`\lambda`$ it was both $`x`$ and $`u`$. Thus, intuitively, with $`u^\epsilon`$ we can get $`x^\epsilon`$ forward and then we can solve for $`lambda`$ backwards. We can do it again and again until we reach an acceptable error against $`u^*`$. This poses the question of existence of $`u^*`$. Otherwise, it will be an infinite loop. We will discuss about Forward-Backward Sweep Method later.

# Sufficient Conditions of an Optimal Control
In order to understand the difference between necessary conditions $`N`$ and sufficient conditions $`S`$ of optimality $`O`$, you can think of it in terms of subsets: $`S \subseteq O \subseteq N \subseteq U`$, where in $`U`$ not all elements may satisfy the necessary conditions. This limits the scope of our discussion. This also explains why all necessary conditions above will be seen here, or a more specific case of them.

## Existence
Consider

```math
\max_u J(u) = \int_{t_1}^{t^2} f(t,x(t),u(t)) dt
```
```math
\begin{array}{rl}
\text{subject to} &
  x^\prime(t) = g(t, x(t), u(t)) \\
& x(t_0) = x_0 \text{ and } x(t_1) \text{ free.}
\end{array}
```

Suppose that $`f(t,x,u)`$ and $`g(t,x,u)`$ are both continuously differentiable functions in their three arguments and concave in $`x`$ and $`u`$. Suppose $`u^*`$ is a control with associated state $`x^*`$, and $`\lambda`$ a piecewise differentiable function, such that $`u^*`$, $`x^*`$, and $`\lambda`$ together satisfy on $`t_0 \leq t \leq t_1`$:
```math
\begin{array}{c}
f_u + \lambda g_u = 0,
\lambda^\prime = - (f_x + \lambda g_x),
\lambda(t_1) = 0,
\lambda(t) \geq 0.
\end{array}
```

Then for all controls u, we have
```math
J(u^*) \geq J(u).
```