Table of contents

1. Review
1.1. Fundamental Theory of Calculus
1.2. Runge-Kutta 4 Method
2. The Optimal Control Theory
2.1. Basic Optimal Control Problem
2.2. Necessary Conditions
2.3. Sufficient Conditions
2.4. Bounded Optimal Control Problem
3. Methodologies
3.1. Forward-Backward Sweep Method
4. Application to Biological Model
4.1. The Problem
4.2. Governing Equations
4.3. The Optimal Control Problem
5. Codes
5.1.
