# Piecewise Continuous
Let $`I \subset \mathbb{R}`$ be an interval (finite or infinite). We say a finite-valued function $`u: I \mapsto \mathbb{R}`$ is *piecewise continuous* if it is continuous at each $`t \in I`$, with possible exception of at most a finite number of $`t`$, and if $`u`$ is equal to either its left or right limit at every $`t \in I`$.
# Piecewise Differentiable
Let $`x: I \mapsto \mathbb{R}`$ be continuous, and $`I`$ is differentiable at all but finitely points of $`I`$. Further, suppose that $`x^\prime`$ is continuous wherever it is defined. Then, we say $`x`$ is piecewise differentiable.
# Continuously Differentiable
Let $`k:I\mapsto \mathbb{R}`$. We say that k is continuously differentiable if $`k^\prime`$ exists and is continuous on $`I`$.
# Concavity
## Concave
A function $`k(t)`$ is said to be concave on $`[a,b]`$ if
```math
\alpha k(t_1) + (1-\alpha)k(t_2) \leq k\left(\alpha t_1 + (1-\alpha)t_2\right)
```
for all $`0\leq\alpha\leq1`$ and for any $`a\leq t_1,t_2\leq b`$.
## Convex
A function $`k(t)`$ is said to be convex if
```math
\alpha k(t_1) + (1-\alpha)k(t_2) \geq k\left(\alpha t_1 + (1-\alpha)t_2\right)
```
for all $`0\leq\alpha\leq1`$ and for any $`a\leq t_1,t_2\leq b`$;
or equivalently, if $`-k`$ is concave.

## Tangent line property
If $`k`$ is differentiable, then we a tangent line property:
```math
k(t_2)-k(t_1) \geq (t_2 - t_1)k^\prime(t_2)
```
if $`k`$ is concave;
```math
k(t_2)-k(t_1) \leq (t_2 - t_1)k^\prime(t_2)
```
if $`k`$ is convex.
