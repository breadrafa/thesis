# miR451 and AMPK Mutual Antagonism in Glioma Cell Migration and Proliferation: A Mathematical Model
Yangjin Kim, Soyeon Roh, Sean Lawler, Avner Friedman
# Abstract
Glioblastoma multiforme (GBM) is the most common and the most aggressive type of brain cancer; the median survival time from the time of diagnosis is approximately one year. GBM is characterized by the hallmarks of rapid proliferation and aggressive invasion. miR-451 is known to play a key role in glioblastoma by modulating the balance of active proliferation and invasion in response to metabolic stress in the microenvironment. The present paper develops a mathematical model of GBM evolution which focuses on the relative balance of growth and invasion.In the present work we represent the miR-451/AMPK pathway by a simple model and show how the effects of glucose on cells need to be "refined" by taking into account the recent history of glucose variations. The simulations show how variations in glucose significantly affect the level of miR-451 and, in turn, cell migration. The model predicts that oscillations in the levels of glucose increase the growth of the primary tumor. The model also suggests that drugs which upregulate miR-451, or block other components of the CAB39/AMPK pathway, will slow down glioma cell migration. The model provides an explanation for the growth-invasion cycling patterns of glioma cells in response to high/low glucose uptake in microenvironment _in vitro,_ and suggests new targets for drugs, associated with miR-451 upregulation.

## Introduction
Glioblastoma is the most common and aggressive brain tumour, marked by high levels of proliferation angiogenesis and invasion <span title="Furnari F, Fenton T, Bachoo R, Mukasa A, Stommel J, et al. (2007) Malignant astrocytic glioma: genetics, biology, and paths to treatment. Genes Dev 21:2683-710">\[[1](https://www.ncbi.nlm.nih.gov/pubmed/17974913)\]</span>. These tumors are highly invasive and the median survival time from the time of diagnosis is approximately one year. Tumor cells may face hypoxia, acidity, and limited nutrient availability as they grow. In order to maintain rapid growth, glioblastoma cells must remain highly adaptive in response to changes in the microenvironment <span title="Godlewski J, Nowicki M, Bronisz A, Palatini GNJ, Lay MD, et al. (2010) Microrna-451 regulates lkbl/ampk signaling and allows adaptation to metabolic stress in glioma cells. Molecular Cell 37: 620-632.">\[[2](https://www.ncbi.nlm.nih.gov/pubmed/20227367)\]</span>. To enable rapid proliferation, cancer cells shift their metabolic machinary toward high levels of glucose uptake and lactate production (the "Warburg Effect") <span title="Warburg O (1956) On the origin of cancer cells. Science 123: 309-14.">\[[3](https://www.ncbi.nlm.nih.gov/pubmed/13298683)</span>,<span title="Kim J, Dang C (2006) Cancer's molecular sweet tooth and the warburg effect. Cancer Res 66:8927-30.">[4](https://www.ncbi.nlm.nih.gov/pubmed/16982728)</span>\]. While differentiated cells favor oxidative phosphorylation and anaerobic glycolysis, proliferative tumor cells adapt what appears to be a less effective way of metabolism, _i.e._, aerobic glycolysis <span title="">\[[5]()\]</span>; see Figure 1. While the tricarboxylic acid (TCA), or Krebs, cycle (schematically described by the pathway in Figure 1) is a key step for generating ATP in nonhypoxic normal cells, cancerous cells consume large amounts of glucose and generate lactic acid rather than using the TCA cycle <span title="Kim J, Dang C (2006) Cancer's molecular sweet tooth and the warburg effect. Cancer Res 66:8927-30.">\[[4](https://www.ncbi.nlm.nih.gov/pubmed/16982728)\]</span>. Cancer cells ensure an adequate glucose supply through increased angiogenesis and migration <span title="Godlewski J, Nowicki M, Bronisz A, Palatini GNJ, Lay MD, et al. (2010) Microrna-451 regulates lkbl/ampk signaling and allows adaptation to metabolic stress in glioma cells. Molecular Cell 37: 620-632.">\[[2](https://www.ncbi.nlm.nih.gov/pubmed/20227367)\]</span> and cellular responses to glucose withdrawal may be critical for the survival of cancer cells in rapidly growing tumors such as glioblastoma, where glucose levels may fluctuate. Cancer cells therefore engage strategies of metabolic adaptation to survive periods of metabolic stress and maintain viability as cells accumulate <span title="">\[[6]()\]</span>. The 5\tick-adenosine monophosphate activated protein kinase (AMPK) pathway is the major cellular sensor of energy availability <span title="">\[[7]()\]</span>. AMPK is a conserved cellular energy sensor that is activated by metabolic stress to promote energy conservation and glucose uptake <span title="">\[[7]()\]</span>. This allows cells to adapt to periods of low energy availability, thus avoiding bioenergetic catastrophe and cell death.

In addition to rapid proliferation, invasion of glioma cells from the core tumor into the surrounding brain tissue is a major reason for treatment failure: the migrating cells are not eliminated in surgical resection and promote tumor recurrence. Godlewski _et al_. <span title="Godlewski J, Nowicki M, Bronisz A, Palatini GNJ, Lay MD, et al. (2010) Microrna-451 regulates lkbl/ampk signaling and allows adaptation to metabolic stress in glioma cells. Molecular Cell 37: 620-632.">\[[2](https://www.ncbi.nlm.nih.gov/pubmed/20227367)\]</span> have recently identified a novel mechanism in which glioma cell survival, motility and proliferation are coordinately regulated by a single microRNA (miR-451) that regulates AMPK signaling in response to glucose levels in glioblastoma cells. Their model is shown schematically in Figure 2. In this study, miRNA-451 was shown to blay a key role in glioblastoma by modulating the balance of active proliferation and invasion in response to metabolic stress in the microenvironment. Normal glucose levels allow high miR-451 which in turn leads to elevated proliferation and decreased cell migration, whereas the low glucose level downregulates miR-451 thereby leading to enhanced cell motility and migration.
Kim _et al._ <span title="">\[[8]()\]</span> recently developed a mathematical model of invasive glioma cells in 3D tumor spheroids. The model was shown to be capable of reproducing migration patterns of glioma cells in _in vitro_ experiments, exhibiting, in particular, dispersion and branching of cells. The model included MMP activity and glucose levels as well as chemotaxis, haptotaxis and cell-cell adhesion forces. The rapid migration of cells is caused primarily by the chemotaxis forces that are associated with glucose concentration _G_.
In the present paper we explore in more detail the effect of glucose on glioma cell behavior with the aim of suggesting drug targets that will slow cell migration. In the present work we represent the miR-451/AMPK pathway by a simple model and show how the effects of glucose on cells need to be "refined" by taking into account the recent history of glucose input to vary periodically. The simulations show how variations in glucose significantly affect the level of miR-451 and, in turn, cell migration. Importantly, the model predicts that oscillations in the levels of glucose increase the growth of the primary tumor. The model also suggests that drugs which upregulate miR-451, or block other components of the CAB39/AMPK pathway, will slow down glioma cell migration.

## Materials and Methods
### Mathematical modeling of miR-451-AMPK control
In order to incorporate the signaling network shown in Figure 2 into our model of glioma cell migration, we began by simplifying this network. We represent by _M_ and _A_ the activities of miR-451 and AMPK complex, respectively. We also denote the signaling pathways to miR-451 and AMPK complex, by _G_ (glucose) and _S_ respectively. Figure 3, with protein degradation at rates $`\mu_1`$ and $`\mu_2`$ (for _M_ and _A_), is simplified representation of Figure 2. The scheme includes autocatalytic activities of _M_ and _A_ as reported in <span title="Godlewski J, Nowicki M, Bronisz A, Palatini GNJ, Lay MD, et al. (2010) Microrna-451 regulates lkbl/ampk signaling and allows adaptation to metabolic stress in glioma cells. Molecular Cell 37: 620-632.">\[[2](https://www.ncbi.nlm.nih.gov/pubmed/20227367)\]</span> and the inhibiting signal suggested in Figure 2A.

To mathematically model the dynamic network shown in Figure 3 we introduce the following variables:

```math
\begin{aligned}
& M(t)=\text{ concentration of miR-451 at time }t,\\
& A(t)=\text{ concentration of the complex CAB39/LKB1/AMPK at time }t,\\
&S=\text{ source of the complex }A,\\
&G=\text{ glucose input level},\\
&\alpha=\text{ inhibition strength of miR-451 by the complex},\\
&\beta=\text{ inhibition strength of the complex }A \text{ by miR-451},\\
&k_1=\text{ autocatalytic enhancement rate of miR-451},\\
&k_2=\text{ autocatalytic enhancement rate of the complex}.
\end{aligned}
```

We associate to the network in Figure 3 the dynamical system written in dimensionless form,

```math
\frac{dM}{\partial t} = G + \frac{k_1k_2^2}{k_2^2+\alpha A^2}-M, (1) 
```
```math
\epsilon\frac{dA}{dt}=S+\frac{k_3k_4^2}{k_4^2+\beta M^2} - \Lambda. (2)
```

Table 1 summarizes the paramteres used in equations (1)-(2), and some explanations are given in the Text S1. The strength of inhibition of miR-451 by the complex _A_ can be controlled through the dimensionless parameter $`\alpha`$, and the strength of the inhibition of the complex _A_ by miR-451 can be controlled through the dimensionless parameter $`\beta`$. The parameter $`\epsilon`$ is given by $`\mu_1/\mu_2`$, the ratio of the degradation rates of _M_ and _A_, respectively. Since miR-451 degrades much faster than the complex _A_, $`\epsilon`$ is a very small number (we shall take $`\epsilon = 0.02`$). We recall (see Figure 2) that elevated levels of miR-451 imply elevated cell proliferation and reduced migration, whereas low levels of miR-451 imply reduced cell proliferation and increased migration. In order to incorporate the effect of glucose into our model of glioma cell migration, we need to determine how the level of glucose _G_ affects the level of _M_.

When the main miR-451-AMPK system (1)-(2) is in equilibrium, we can solve miR-451 levels (M) as a function of injected glucose amounts (G) for any set of parameters $`S,k_i,\alpha,\beta`$. Figure 4 shows the graph $`M=M(G)`$ as $`S`$-shaped curve when the parameter values are $`k_1=4.0, k_2= 1.0, \alpha=1.6,k_3=4.0,k_4=1.0,\beta=1.0, S=0.2,\epsilon = 0.02`$. The upper and lower branches are stable, and the middle branch is unstable. If _G_ is small, then the system (1)-(2) is in the lower branch, _M_ is low, and the glioma cells are in migratory phase. This situation continues to hold as G is increased until it reaches the value 0.6. At this point, the system jumps to the high branch, with an elevated level of miR-451, and the cells are in the proliferative phase (while migration is reduced). As _G_ is decreased, the level of miR-451 remains elevated, until _G_ is decreased to the level of 0.4, at which the miR-451 jumps to the lower branch, and the cells return to the migratory phase. We at an intermediate level, $`0.4<G<0.6`$, the cells are in the migratory phase if _G_ was in increasing mode, and in the proliferative phase if _G_ was in decreasing mode.

Figure 4 suggests that a state $`(G,M)`$ with $`M=2.0`$ will be moved by the dynamical system (1)-(2) into the upper stable steady state branch, resulting in overexpression of miR-451 and, thus, in growth. On the other hand, if $`M<2.0`$, then $`(G,M)`$ will end up in the lower stable state branch, which implies migration. Hence we shall define the migratory region by $`M<th_M`$ and the proliferation/growth region by $`M>th_M`$ and take the threshold $`th_M=2.0`$.

In the next section we shall incorporate this phenomenon into the mmodel which was developed in Kim _et al._ \[8\]. We conclude this section by comparing predictions of the model (1)-(2) with experimental results of Godlewski _et al._ <span title="Godlewski J, Nowicki M, Bronisz A, Palatini GNJ, Lay MD, et al. (2010) Microrna-451 regulates lkbl/ampk signaling and allows adaptation to metabolic stress in glioma cells. Molecular Cell 37: 620-632.">\[[2](https://www.ncbi.nlm.nih.gov/pubmed/20227367)\]</span>. In the experiments, the miR-451 level was reduced by 80% when cells were cultured in low glucose ($`0.3\;g/l`$) compared normal glucose ($`4.5\;g/l`$). Our simulation results, shown in Figure 5, are in good agreement with these experimental results. Indeed, with the parameter set as in Figure 4, the model predicts slightly more than 80% reduction of miR-451 levels in low glucose.

#### Mathematical model of the complete dynamics
In \[8\] we previously developed a mathematical model of migration patterns of glioma cells. The model included the forces of cell-cell adhesion, haptotaxis, chemotaxis induced by a glucose gradient, and shedding of glioma cells from a spheroid glioma tumor.
In the present paper our aim is to examine in more detail the effect of glucose on cell migration. In order to elucidate the dependence of glioma cell migration on the gradient glucose _G_, we whall vary _G_ in a periodic manner at a location away from the primary tumor and determine how these changes affect the proliferative and migratory phases of the cells. In doing so we may ignore the shedding of cells, as this will not affect how fast cells migrate after they leave the primary tumor. We may also ignore the cell-cell adhesion, since this force only affects the clumping pattern of cells, not their proliferation rate or speed of migration. Thus in order to describe the effect of _G_-oscillation on migratory/proliferative phases of glioma cells, we ignore shedding and cell-cell adhesion, but include haptotaxis and chemotaxis. On the other hand we shall incorporate the observation (modeled by (1)-(2)) that tumor cells react ot miR-451 expression in response to the glucose level.

The model simulation will show that the effect of glucose oscillations is schematically captured by Figure 6: The same total amount of glucose, if injected in a fluctuating fashion, would lead to increased tumor growth.

#### Tumor cell density ($`=n(x,t)`$)
As noted in the previous Section, tumor cells begin to invade the surrounding environment when miR-451 levels are low and AMPK activity is high, and they proliferate when miR-451 levels are high and AMPK activity is low. This alternating behavior between migration and proliferation under the control of miR-451 is modeled using a threshold of miR-451 level, $`th_M`$, by the following equation

```math 
\frac{\partial n}{\partial t} = \underbrace{-(\nabla\cdot J_n)I_{M<th_M}}_{Migration} + \underbrace{ P_nI_{M>th_M}}_{Proliferation}, (3) 
```
where $`J_n`$ is the flux of the cells under the migration mode, $`P_n`$ is the proliferation term under the growth mode, and $`I_{M>th_M}`$ and $`I_{M<th_M}`$ are indicator functions. They are discontinuous but made smooth in simulations.

The flux for movement consists of three components: random motility ($`J_m`$), chemotaxis ($`J_c`$), and haptotaxis ($`J_h`$).

```math 
J_n = J_m + J_c + J_h 
```
```math 
J_n = \underbrace{-D_n\nabla n}_{Random\;motility} + \underbrace{\left(\chi_n\frac{n\nabla G}{\sqrt{1+\lambda_G\left|\nabla G\right|^2}} \right)}_{Chemotaxis} + \underbrace{\left(\chi_n^1\frac{n\nabla\rho}{\sqrt{1+\lambda_\rho\left|\nabla\rho\right|^2}} \right)}_{Haptotaxis} (4) 
```
where $`D_n`$ is the random motility, $`\chi_n`$ is the chemotactic sensitivity, $`\chi_n^1`$ is the haptotactic coefficient, and $`\lambda_G`$ and $`\lambda_\rho`$ are chemotactic and haptotactic parameters respectively. $`P_n `$ represents a logistic growth under proliferation mode:

```math 
P_n = \lambda_{11}n(1-n/n_0). 
```

#### ECM ($`=\rho(x,t)`$)
ECM is degraded by MMP and undergoes remodeling/reconstruction,

```math 
\frac{\partial\rho}{\partial t} = -\lambda_{21}P_\rho + \lambda_{22}\rho(1-\rho/\rho_0), (5) 
```
where $`\lambda_{21}`$ is the ECM degradation rate by MMP, $`\lambda_{22}`$ is the reconstruction parameter, and $`\rho_0`$ is the ECM carrying capacity.

#### MMP ($`=P(x,t)`$)
MMP is produced by invasive tumor cells in the presence of the ECM and is usually localized near the invading cell front (small diffusion),

```math 
\frac{\partial P}{\partial t} = D_P\Delta P+\lambda_{31}n\rho - \lambda_{32}P, (6) 
```
where $`D_P`$ is the diffusion coefficient, $`\lambda_{31}`$ is MMP production by tumor cells ($`n`$) and $`\lambda_{32}`$ is the natural decay rate.

#### Glucose ($`=G(x,t)`$)
To define the oscillation of the glucose supply, we denote by $`\Omega`$ a region which includes all the glioma cells, and by $`\Omega_\delta`$ a subregion which lies far away from the initial location of the glioma cells. We choose discrete times $`t_j(1\leq j \leq N)`$ with a period of $`\tau`$ (that is, $`t_j - t_{j-1} = \tau`$ for all $`j`$) and introduce the indicator functions

```math 
I_j(x,t)=\left\{ 
\begin{array}{lc}
1 & if\; x\in\Omega_\delta, t_j\leq t\leq t_j + \delta^\prime\\
2 & otherwise
\end{array}\right. (7) 
```
where N is the number of injections that take place during intervals $`t_j < t<t_j+\delta^\prime (\delta^\prime < \tau>)`$. We assume that the concentration of glucose ($`G`$) satisfies a reaction-diffusion equation on the whole domain $`\Omega`$:

```math 
\frac{\partial G}{\partial t}=\underbrace{D_G\Delta G}_{Diffusion}-\underbrace{\lambda_{41}nG}_{Consumption} + \sum_{j=1}^N \lambda_{42}I_j, (8) 
```
where $`D_G`$ is the diffusion coefficient, $`\lambda_{41}`$ is the consumption rate of glucose by tumor cells, and the last term is the glucose supply rate from the far away field.

#### miR-451 ($`=M(x,t)`$) and AMPK ($`=A(x,t)`$)
The two internal key variables $`M,A`$ (miR-451 and AMPK) for the control of growth and invasion at each tumor cell are linked to tumor cell density above ($`n`$):

```math
\frac{\partial M}{\partial t} = \left(G+ \frac{k_1k_2^2}{k_2^2+\alpha A^2}-M\right)\frac{n}{n_0}, (9) 
```

```math
\epsilon\frac{\partial A}{\partial t} = \left(S+\frac{k_3k_4^2}{k_4^2+\beta M^2}-A\right)\frac{n}{n_0}. (10)
```

#### Governing equations
Collecting the equations derived above for the tumor density ($`n(x,t)`$), concentrations of the ECM ($`p(x,t)`$), MMP ($`P(x,t)`$), glucose ($`G(x,t)`$), miR-451 ($`M(x,t)`$), and AMPK ($`A(x,t)`$), we have

```math

\begin{aligned}
\frac{\partial n}{\partial t} = & \left[D_n\Delta n - \nabla\cdot\left(\chi_n\frac{n\nabla G}{\sqrt{1+\lambda_G\left|\nabla G\right|^2}}\right)
-\nabla\cdot\left(\chi_n^1\frac{n\nabla \rho}{\sqrt{1+\lambda_\rho\left|\nabla \rho\right|^2}}\right)\right]I_{M<th_M} \\ & 
+ \lambda_{11}n(1-n/n_0)I_{M>th_M}, (11)
\end{aligned}
```

```math
\frac{\partial \rho}{\partial t} = -\lambda_{21}P\rho+\lambda_{22}\rho(1-\rho/\rho_0), (12)
```

```math
\frac{\partial P}{\partial t} = D_P\Delta P+\lambda_{31}n\rho - \lambda_{32}P, (13)
```

```math
\frac{\partial G}{\partial t} = D_G\Delta G - \lambda_{41}nG+\sum_{j=0}^{N-1}\lambda_{42}I_j, (14)
```

```math
\frac{\partial M}{\partial t} = \left(G+\frac{k_1k_2^2}{k_2^2+\alpha A^2}-M\right)\frac{n}{n_0}, (15)
```

```math
\epsilon\frac{\partial A}{\partial t} = \left(S+\frac{k_3k_4^2}{k_4^2+\beta M^2}-A\right)\frac{n}{n_0}, (16)
```
where $`I_{M>th_M}`$ is the indicator function of growth region (where miR-451 level ($`M`$) is greater than the threshold value ($`th_M`$)) and $`I_{M<th_M}`$ is the indicator function of invasive region (where miR-451 level is below the threshold ($`th_M`$)).

The parameter values and the reference values for equations (11)-(14) are given in Tables 2 and 3, respectively. Some of the parameters are taken from the literature and others are estimated. Text S1 explains the choice of the paramters and reference values, and the non-dimensionalzation scheme.

All the simulations in the Results section were performed using a finite volume method and clawpack (http://www.amath.washington.edu/~claw/) with a fractional step method [9] as well as the non-linear solver _nksol_ for the algebraic systems. Equations (11)-(16) were solved on a regular uniform spatial grid ($`h_x = 0.01`$). An initial time step of $`dt = 0.0001`$ was used, but adaptive time stepping based on the number of iterations did increase or decrease the step size.

### Results
#### Key control system : miR-451-AMPK network
Consider a spherical brain tissue, $`\left|x\right| < R`$, with glioblastoma tumor occupying 2 sphere $`\left|x\right| < R_0`$ and glucose source at $`R-\delta <\left|x\right| < R(R-\delta > R_0)`$. Glucose is consumed by tumor cells, resulting in low glucose concentrations near $`\left|x\right|=R_0`$ and relatively high glucose concentrations near the far field $`\left|x\right| = R`$ . This creates a gradient field of glucose. Under this microenvironmental condition, the glioblastoma cells tend to migrate toward glucose rich region, _i.e._, towards the far field, $`\left|x\right| = R`$. Indeed, glioblastoma cells are known for their particular tendency to metabolize glucose, through aerobic glycolysis, called the Warburg effect; recall Figure 1. Furthermore, the cells in the tumor core, starving and accumulating toxic waste materials, are sending 'escape' messages through hand-hand signaling toward the cells at the surface $`\left|x\right|= R_0`$ of the tumor, further encouraging them to invade into the far field. In our model low levels of miR-451 (high level of AMPK activity) due to low glucose levels at cell sites trigger tumor cells to initiate invasion toward $`\left|x\right|=R`$, and keep invading until the miR-451 level creeps above a threshold ($`th_M=2.0`$) (or AMPK activity level drops below a threshold ($`th_A=2.0`$)). For simplicity we carry out the simulations of the model equations (11)-(16) in the one-dimensional case. The computational domain is $`\Omega = \left\{0<x<1\right\}`$, and we take $`\Omega_\delta = \left\{1-\delta\leq x\ \leq 1\right\}`$. The glioma cells begin to migrate into $`\Omega`$ from the end-point $`x=0`$. Glucose is consumed by tumor cells initially on the left side of the domain leading to low glucose concentrations near $`x=0`$ and relatively high glucose concentrations in the far field (near $`x=1`$).

#### Simulation results
Figure 7 shows a typical time course of tumor density ($`n`$) and concentrations of ECM($`\rho`$), MMPs ($`P`$), glucose ($`G`$), miR-451 ($`M`$), and AMPK ($`A`$) in response to a periodic injection of glucose into the system. Tumor cells were initially located on the left-hand side of the domain $`[0,1]`$, near $`x=0`$. Glucose is consumed by tumor cells creating a gradient of glucose with higher levels at more distant areas. This lowered glucose level induces low miR-451 levels and high AMPK activity. Tumor cells near the surface of the tumor mass (with cell density $`< 10\%`$) begin to invade into the medium (toward the right) through chemotaxis (migration toward gradient of glucose) and haptotaxis (migration toward gradient of ECM using MMPs). MMPs are localized near moving front cells, and the high level of MMPs degrades ECM. This invasion continues until another flux of glucose is introduced to the system. Glucose injections at discrete times $`[t_j,t_j + \delta^\prime],\;j=1,2,3`$ ($`t_1=20,\;t_1 = 50,\;t_3=80`$; $`\delta^\prime=0.01;\;\tau=30`$), then induce a high level of miR-451 (low AMPK activity) enabling tumor cells in the invasive region to begin to grow again. One can observe fluctuating miR-451 levels and AMPK activities in response to glucose levels.

Figure 8 show a time course of total tumor population and the levels of glucose, miR-451, and AMPK. Fluctuating glucose levels in ($`A`$) lead to a peak of miR-451 level and low AMPK activity. In turn, fluctuating AMPK levels give rise to plateau invasion mode (marked as black arrow in (A)) and creeping growth curve (red arrow in ($`A`$)) of the tumor population due to invading cells near the tumor surface when the AMPK level is high.

Figure 9 shows that when the total supply of glucose is fixed the growth rate of the tumor with glucose fluctuation (black solid line) is larger than one with fixed supply of glucose (dotted black line).

In Figure 10, we show the effect of inhibition strength ($`\alpha`$) of miR-451 by the AMPK complex (more specifically, by the LKB1/STRAD complex; see Figure 2A) on the tumor population. In contrast to the control case (marked as star ($`*`$)) the tumor population does not fluctuate (Figure 10A) and shows slower growth as $`\alpha`$ is decreased. Lower values of inhibition strength $`\alpha`$ mean higher levels of miR-451 which induce low AMPK activity in Figure 10B. Blocking miR-451 along the pathways from CAB39/LKB1/STRAD/AMPK to miR-451 could be therfore a possible therapeutic target.

In Figure 11, we show the effect of inhibition strength ($`\beta`$) of AMPK complex by miR-451 on the tumor population. In contrast to the control case (marked as star ($`*`$)), the tumor population does not show invasion-growth patterns (Figure 11) for large $`\beta`$ value and shows slower growth as $`\beta`$ is increased. For intermediate values of $`\beta=1.2`$, periodic fluctuation of AMPK is observed but the duration of high AMPK level is not as long as on the case of control ($`\beta=10`$). We also see, in the case of $`\beta=1.2`$, shorter invasion periods and slower growth than in the control case. Higher values of inhibition strength $`\beta`$ mean lower AMPK activity (in Figure 11B) which also induces high values of miR-451 and a much smaller tumor population. So increasing inhibition of AMPK by miR-451 would have the same effect as decreasing inhibition of miR-451 by the AMPK pathway, and this suggests another possible therapeutic target.

In Figure 12, we show the time evolution of tumor population and total concentrations of AMPK and miR-451 for different values of inection period ($`\tau`$) and injection amount (\lambda_{42}). In the case of larger amounts of injection (red; $`\lambda_{42}=30`$), the overall tumor population grows faster than the smaller injection amount cases (black; $`\lambda_{42}=5`$). In the case of smaller amounts of injection, the low glucose level ($`\tau=50`$) induces constant invasion without growth phase (black dotted line). In comparison, the frequent onjection of glucose ($`\tau = 10`$) into the system creates a growth-invasion pattern. However, when te glucose is introduced into the system every ten hours ($`\tau=10`$) the system (of glucose and tumor cells) undergoes only three cycles of invasion and growth in 100 hours. This could be explained by the low miR-451 concentrations and high AMPK activity in Figure 12. In the case of larger amounts of glucose injected ($`\lambda_{42}=30`$), frequent injection leads to monotone growth phase while less frequent but appropriately small amounts of glucose injection lead to faster growth through cycles of invasion and growth. In the former case, as one can see in low AMPK concentration, tumor cells spend most of their time in growth phase due to abundant glucose with frequent large amounts of glucose injection. In the latter case, we also note that the number of injections matches with the corresponding cycle, _i.e._ the system adats to the growth-invasion phase each time glucose was administered. This dynamic behavior can also be confirmed in highly dynamic fluctuations in concentrations of miR-451 and AMPK.

### Discussion
Glioblastoma cancer cells shift their metabolic machinery toward a high level of glucose uptake, a phenomenon called the Warburg effect. Strong dependency of cells on glucose causes them to migrate in the direction of abundant glucose levels. Thus, the cancer cells migrate in the direction of increased glucose sources. Godlewski _et al._ <span title="Godlewski J, Nowicki M, Bronisz A, Palatini GNJ, Lay MD, et al. (2010) Microrna-451 regulates lkbl/ampk signaling and allows adaptation to metabolic stress in glioma cells. Molecular Cell 37: 620-632.">\[[2](https://www.ncbi.nlm.nih.gov/pubmed/20227367)\]</span> demonstrated that under low glucose levels glioma cells downregulate miR-451, and that this leads to upregulation of the AMPK kinases, which in turn increases the migration of cells while reducing their proliferation and growth. On the other hand, under normal (high) glucose levels, miR-451 is upregulated, leading to decreased AMPK activity, which results in elevated proliferation and growth but reduced migration.

In the present paper we developed a mathematical model of glioma cell migration and proliferation. In this paper, we considered the important role that miR-451 plays in glioblastoma. The model includes the concentrations of cancer cells, ECM, MMP, glucose, miR-451 and AMPK. Simulation of the model shows that by changing the level of glucose through periodic injections of glucose, the cancer cells will undergo nearly periodic changes from migration to proliferation modes. We showed how the effects of glucose on cells need to be "refined" by taking into account the recent history of glucose variations. The simulations show how variations of glucose significantly affect the level of miR-451 and, in turn, cell migration. The model predicts that oscillations in the levels of glucose increase the growth of the primary tumor. The model also suggests that drugs which upregulate miR-451, or block other components of the CAB39/AMPK pathway, will slow down glioma cell migration. In summary, the conclusions from the model include:

(i) Fluctuating glucose can lead to faster tumour growth and spread

(ii) Response depends on the previous glucose levels, _i.e._, whether it is rising or falling.

(iii) Targeting this pathway may reduce tumour growth by disrupting the cyclic responses to fluctuating glucose.

We suggest these simulation results as hypotheses to be tested experimentally. If the predictions of the model can be confirmed experimentally, we could proceed, side-by-side with the model and with experiments to determine how by modulating the injection of glucose, upregulating miR-451 (decreasing the $`\alpha`$ inhibition) and downregulating LKB1/STRAD (increasing the $`\beta`$ inhibition) we can decrease the invasion and the overall growth of the tumor. Simulations suggest that targetng this pathway may be beneficial in GBM treatments.

In this paper we used the concentration of tumor cells, not individual cells. Since even an isolated migrating glioblastoma cell may lead to faster spread and recurrence after surgery, it would be important to extend our paper by introducing cancer cells as individual agents. Hybrid approach will include cancer cells as individuals but the various chemical species as concentrations. A hybrid approach [10] has been proven to be important and useful in order to better understand the detailed mechanical interaction between a tumor cell and its microenvironment (see a recent review [11]). For simplicity we did not include our model some important components of the microenvironment that may affect the growth and migration of glioma, notably fibroblasts, endothelial cells, and immune cells, as well as cytokines and growth factors secreted by these cells. There is currently only a limited understanding of the complex relationship between the tumor cells and the host cells in the microenvironment. A better understanding of this relationship may lead to new therapeutic approaches that target stromal elements instead of, or in addition to, tumor cells. We hope to address these situations in future work.

### Supporting Information

Text S1 Detailed description of the models and parameters used. (PDF)

### Author Contributions
Conceived and designed the experiments: YJK. Analyzed the data: YJK SYR. Contributed reagents/materials/analysis tools: YJK SYR. Wrote the paper YJK SL AF.

### References
