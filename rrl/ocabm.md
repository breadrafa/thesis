### page 110-111
```math
z^prime(t) = h(t,x(t),u(t)),
```
```math
z(t_0)=0,
```
```math
z(t_1)=B.
```

Thus, (12.3) is transformed into
```math
\max_u \int_{t_0}^{t_1} f(t,x(t),u(t))dt + \phi(x(t_1))
```
```math
\begin{array}{rl}
\text{subject to} & x^\prime(t) = g(t,x(t),u(t)), x(t_0) = x_0, \\
& z^\prime(t) = h(t,x(t),u(t)), z(t_0) = 0, z(t_1) = B, \\
& a \leq u(t) \leq b.
\end{array}
```
This problem can now be solved using methods developed in this chapter. We present the following example. For an additional example, see Exercise 12.9.

### Example 12.7
```math
\min_u\frac12\int_0^1 u(t)^2 dt
```
```math
\begin{array}{rl}
\text{subject to}&x^\prime(t)=u(t),x(0)=0,x(1)=1,\\
&\int_0^1x(t)dt=2.
\end{array}
```
Introduce a second state variable z(t) with z^\prime(t)=x(t), z(0)=0, and z(1)=2. Then, the above problem converts to
```math
\min_u\frac12\int_0^1 u(t)^2 dt
```
```math
\begin{array}{rl}
\text{subject to}&x^\prime(t)=u(t),x(0)=0,x(1)=1,\\
&z^\prime(t)=x(t),z(0)=0,z(1)=2.
\end{array}
```
The Hamiltonian will be

```math
H=\frac12 u^2+\lambda_1u+\lambda_2x.
```
The second adjoint equation is
```math
\lambda_2^\prime(t)=-\frac{\partial H}{\partial z}=0,
```
so that $`\lambda_2\cong C`$ for some constant $`C`$. Also, the first adjoint equation yields
```math
\lambda_1^\prime(t)=-\frac{\partial H}{\partial x}=-\lambda_2=-C\implies \lambda_1(t) = k - Ct
```
for some constant $`k`$. The optimality condition is
```math
0=\frac{\partial H}{\partial u} = u+\lambda_1 \implies u^*(t)=-\lambda_1(t)=Ct-k.
```
Using the state equation for $`x`$ and $`x(0)=0`$, we have
```math
x^\prime(t)=u(t)=Ct - k \implies x(t)=\frac C2t^2 - kt.
```
Similarly, sing the state equation for z and z(0)=0, we have
```math
z^\prime(t)=x(t)=\frac C2t^2 - kt\implies z(t)=\frac C6t^3-\frac k2t^2.
```
Finally, using the terminal conditions,
```math
1=x(1)=\frac C2 - k,
```
```math
2=z(1)=\frac C6 - \frac k2,
```
