# Basic Optimal Control Problems
The following is an example of how states relates with each other:
<br>
Assumption: rate is proportional to weight...
```math
x_1^\prime(t) = u(t)x_1(t), 
```
when it comes to vegetative
```math
x_2^\prime(t) = (1-u(t))x_2(t), 
```
and in reproductive part
```math
0 \leq u(t) \leq 1, 
```
based on controlled partition of weights of vegetative is to reproductive
```math
x_1(0) > 0, x_2(0) \geq 0,
```
#### example problem 1

```math
\max_u{\int_0^T{\ln (x_2(t))dt}}
```
subject to the above constraints. note: x_2(t) depends on u.

## Preliminaries
#### Definition 1.1
#### finite-valued function u
#### Definition 1.2
#### piecewise differentiable
#### Definition 1.3
#### continuously differentiable
#### Definition 1.4
#### concave
#### Definition 1.5
#### Lipschitz function and constant
#### Theorem 1.1
#### Mean Value Theorem

## The Basic Problem and Necessary Conditions
## The Pontryagin's Maximum Principle
## Exercises

# Existence and Other Solution Properties

## Existence and Uniqueness Results Interpretation of the Adjoint
## Principle of Optimality
## The Hamiltonian and Autonomous Problems
## Exercises

# State Conditions at the Final Time

## Payoff Terms
## States with Fixed Endpoints
## Exercises

# Forward-Backward Sweep Method

# Lab 1: Introductory Example